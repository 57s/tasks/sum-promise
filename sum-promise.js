function sum(...arg) {
	return new Promise(function (resolve, reject) {
		resolve(
			arg.reduce(
				(sum, number) =>
					(sum += Number.isNaN(Number(number)) ? 0 : Number(number)),
				0
			)
		);
	});
}

sum(10, '40', '', 0, '0', '5t').then(console.log); // 50
